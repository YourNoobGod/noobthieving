package graphics;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class UIHandler extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 571687895306344218L;
	private JPanel contentPane;

	public UIHandler() {
		setTitle("NoobThieving");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 484, 288);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnEntity = new JButton("Entity");
		btnEntity.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							EntityUI frame = new EntityUI();
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});

		btnEntity.setBounds(10, 11, 220, 225);
		contentPane.add(btnEntity);

		JButton btnObjects = new JButton("Objects");
		btnObjects.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							StallUI frame = new StallUI();
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				dispose();
			}
		});
		btnObjects.setBounds(240, 11, 220, 225);
		contentPane.add(btnObjects);
	}
}
