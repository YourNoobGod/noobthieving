package graphics;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.Thieving;
import com.data.Entity;
import com.data.Healing;

public class EntityUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1250349048731526565L;
	private JPanel contentPane;
	private JTextField entityField;
	private JTextField foodField;
	private JButton btnStart;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EntityUI frame = new EntityUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EntityUI() {
		setTitle("Entity");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 216, 195);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblEntityName = new JLabel("Entity Name: ");
		lblEntityName.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblEntityName.setHorizontalAlignment(SwingConstants.CENTER);
		lblEntityName.setBounds(10, 11, 180, 14);
		contentPane.add(lblEntityName);

		entityField = new JTextField();
		entityField.setToolTipText("Enter the NPC name");
		entityField.setFont(new Font("Tahoma", Font.BOLD, 11));
		entityField.setHorizontalAlignment(SwingConstants.CENTER);
		entityField.setText("Man");
		entityField.setBounds(10, 36, 180, 20);
		contentPane.add(entityField);
		entityField.setColumns(10);

		JLabel lblFoodName = new JLabel("Food Name:");
		lblFoodName.setHorizontalAlignment(SwingConstants.CENTER);
		lblFoodName.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFoodName.setBounds(10, 67, 180, 14);
		contentPane.add(lblFoodName);

		foodField = new JTextField();
		foodField.setText("Lobster");
		foodField.setHorizontalAlignment(SwingConstants.CENTER);
		foodField.setFont(new Font("Tahoma", Font.BOLD, 11));
		foodField.setColumns(10);
		foodField.setBounds(10, 92, 180, 20);
		contentPane.add(foodField);

		btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Healing.food = foodField.getText();
				Entity.n = entityField.getText();
				dispose();
				Thieving.uiWait = false;
			}
		});
		btnStart.setBounds(10, 123, 180, 23);
		contentPane.add(btnStart);
	}
}
