package graphics;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.Thieving;
import com.data.Drop;
import com.data.Steal;

public class StallUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3748830869309007724L;
	private JPanel contentPane;
	private JTextField objectField;
	private JTextField dropField;

	/**
	 * Create the frame.
	 */
	public StallUI() {
		setTitle("Objects");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 219, 193);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblStallName = new JLabel("Stall Name");
		lblStallName.setHorizontalAlignment(SwingConstants.CENTER);
		lblStallName.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblStallName.setBounds(10, 11, 180, 14);
		contentPane.add(lblStallName);

		objectField = new JTextField();
		objectField.setText("Tea stall");
		objectField.setHorizontalAlignment(SwingConstants.CENTER);
		objectField.setFont(new Font("Tahoma", Font.BOLD, 11));
		objectField.setColumns(10);
		objectField.setBounds(10, 36, 180, 20);
		contentPane.add(objectField);

		JLabel lblDropName = new JLabel("Drop Name:");
		lblDropName.setHorizontalAlignment(SwingConstants.CENTER);
		lblDropName.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDropName.setBounds(10, 67, 180, 14);
		contentPane.add(lblDropName);

		dropField = new JTextField();
		dropField.setText("Cup of tea");
		dropField.setHorizontalAlignment(SwingConstants.CENTER);
		dropField.setFont(new Font("Tahoma", Font.BOLD, 11));
		dropField.setColumns(10);
		dropField.setBounds(10, 92, 180, 20);
		contentPane.add(dropField);

		JButton startButton = new JButton("Start");
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Drop.toDrop = dropField.getText();
				Steal.objName = objectField.getText();
				dispose();
				Thieving.uiWait = false;
			}
		});
		startButton.setBounds(10, 123, 180, 23);
		contentPane.add(startButton);
	}

}
