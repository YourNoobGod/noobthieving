package com;

import graphics.UIHandler;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics2D;
import java.util.ArrayList;

import com.data.Drop;
import com.data.Steal;
import com.handler.Node;
import com.inubot.api.methods.Skills;
import com.inubot.api.oldschool.Skill;
import com.inubot.api.util.Paintable;
import com.inubot.api.util.StopWatch;
import com.inubot.api.util.Time;
import com.inubot.script.Manifest;
import com.inubot.script.Script;
import com.utils.PaintUtils;

@Manifest(name = "NoobThieving", developer = "Calle", desc = "Supports stalls atm", version = 1.0)
public class Thieving extends Script implements Paintable {

	private ArrayList<Node> nodes = new ArrayList<>();

	private StopWatch runtime;
	private int exp = 0;
	public static boolean uiWait = true;

	@Override
	public void render(Graphics2D g) {
		int gained = Skills.getExperience(Skill.THIEVING) - exp;
		int ttl = Skills.getExperienceToNextLevel(Skill.THIEVING);
		PaintUtils.DrawText(g, "By Calle - NoobThieving 1.0", 12, 40, Color.magenta);
		PaintUtils.DrawText(g, "Time Elapsed: " + runtime.toElapsedString(), 12, 60, Color.magenta);
		PaintUtils.DrawText(g, "Experience Gained: " + gained, 12, 80, Color.magenta);
		PaintUtils.DrawText(g, "Experience left: " + ttl, 12, 100, Color.magenta);
		

	}

	@Override
	public boolean setup() {
		exp = Skills.getExperience(Skill.THIEVING);
	       runtime = new StopWatch(0);
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						UIHandler frame = new UIHandler();
						frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			
			while (uiWait) {
				Time.sleep(50);
			}
			
		return true;
	}

	@Override
	public int loop() {
		nodes.add(new Steal());
		nodes.add(new Drop());
		try {
			for (Node s : nodes) {
				if (s.validate()) {
					s.execute();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 50;
	}

}
