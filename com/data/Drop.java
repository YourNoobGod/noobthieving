package com.data;

import com.handler.Node;
import com.inubot.api.methods.Inventory;
import com.inubot.api.util.filter.Filter;

public class Drop extends Node {
	
	public static String toDrop = null;

	@Override
	public boolean validate() {
		return Inventory.contains(toDrop);
	}

	@Override
	public void execute() {
		Inventory.dropAll(Filter.always());
	}

}
