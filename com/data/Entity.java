package com.data;

import com.handler.Node;
import com.inubot.api.methods.Npcs;
import com.inubot.api.methods.Players;
import com.inubot.api.oldschool.Npc;

public class Entity extends Node {
	
	public static boolean entitySelected = false;
	public static String n;
	Npc t;

	@Override
	public boolean validate() {
		return entitySelected && !Players.getLocal().isMoving() && Players.getLocal().getAnimation() == -1;
	}

	@Override
	public void execute() {
		 t = Npcs.getNearest(n);
		 if(t.distance() <10) {
			 t.processAction("Pick");
		 }
	}

}
