package com.data;

import com.handler.Node;
import com.inubot.api.methods.GameObjects;
import com.inubot.api.methods.Inventory;
import com.inubot.api.methods.Players;
import com.inubot.api.oldschool.GameObject;

public class Steal extends Node{
	
	public static String objName = null;

	@Override
	public boolean validate() {
		return !Inventory.contains(Drop.toDrop) && Players.getLocal().getAnimation() == -1;
	}

	@Override
	public void execute() {
        GameObject s = GameObjects.getNearest(obj -> {
            if (obj != null) {
                if (obj.getName() != null) {
                    if (obj.getName().equals(objName)) {
                        return true;
                    }
                }
            }
            return false;
        });
        if(s != null) {
        	s.processAction("Steal-from");
        }
	}
	

}
