package com.data;

import com.handler.Node;
import com.inubot.api.methods.Inventory;
import com.inubot.api.methods.Players;

public class Healing extends Node{
	
	public static String food = "Lobster";

	@Override
	public boolean validate() {
		return Players.getLocal().getHealth() <= 50 && Inventory.contains(food);
	}

	@Override
	public void execute() {
		Inventory.getFirst(food).processAction("Eat");
	}

}
